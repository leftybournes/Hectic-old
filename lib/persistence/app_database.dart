// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:synchronized/synchronized.dart';

import '../utils/strings.dart';
import 'plan.dart';

class AppDatabase {
  static AppDatabase _appDb = AppDatabase._internal();
  AppDatabase._internal();
  Database _db;
  String dbPath;
  final _lock = Lock();

  static AppDatabase get() {
    return _appDb;
  }

  Future<Database> _initDb() async {
    String dbDirectory = await getDatabasesPath();
    dbPath = join(dbDirectory, Strings.dbName);
    await _lock.synchronized(() async {
      _db = await openDatabase(
        dbPath,
        version: 1,
        onCreate: _onCreate,
      );
    });

    return _db;
  }

  Future<Null> _onCreate(Database db, int version) async {
    await db.execute('''CREATE TABLE IF NOT EXISTS ${Plan.tableName} (
        ${Plan.columnId} INTEGER PRIMARY KEY NOT NULL,
        ${Plan.columnDetails} TEXT NOT NULL,
        ${Plan.columnNotes} TEXT,
        ${Plan.columnStartTime} INTEGER,
        ${Plan.columnEndTime} INTEGER,
        ${Plan.columnDateScheduled} INTEGER NOT NULL,
        ${Plan.columnDateCreated} INTEGER NOT NULL,
        ${Plan.columnIsReminderSet} INTEGER NOT NULL,
        ${Plan.columnReminderSchedule} INTEGER
      )''');
  }

  Future<Null> close() async => _db.close();

  Future<Database> _getDb() async {
    if (_db != null) return _db;

    return _initDb();
  }

  Future<Plan> insertPlan(Plan plan) async {
    var db = await _getDb();
    plan.id =
        await db.transaction((txn) => txn.insert(Plan.tableName, plan.toMap()));

    return plan;
  }

  Future<Plan> updatePlan(Plan plan) async {
    var db = await _getDb();
    plan.id = await db.transaction((txn) => txn.update(
          Plan.tableName,
          plan.toMap(),
          where: '${Plan.columnId} = ?',
          whereArgs: [plan.id],
        ));

    return plan;
  }

  Future<int> deletePlan(int id) async {
    var db = await _getDb();
    final result = await db.transaction((txn) => txn.delete(Plan.tableName,
        where: '${Plan.columnId} = ?', whereArgs: [id]));

    return result;
  }

  Future<List<Plan>> getPlansByDate(int date) async {
    var db = await _getDb();
    List<Map> maps = await db.query(
      Plan.tableName,
      columns: [
        Plan.columnId,
        Plan.columnDetails,
        Plan.columnNotes,
        Plan.columnStartTime,
        Plan.columnEndTime,
        Plan.columnIsReminderSet,
        Plan.columnDateScheduled,
        Plan.columnDateCreated,
        Plan.columnReminderSchedule,
      ],
      where: '${Plan.columnDateScheduled} = ?',
      whereArgs: [date],
      orderBy: Plan.columnStartTime,
    );

    List<Plan> plans = [];

    if (maps != null && maps.length > 0) {
      for (Map map in maps) {
        plans.add(Plan.fromMap(map));
      }

      return plans;
    }

    return null;
  }
}
