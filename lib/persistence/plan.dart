// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

class Plan {
  int id;
  String details;
  String notes;
  int startTime;
  int endTime;
  bool isReminderSet;
  int reminderSchedule;
  int dateScheduled;
  int dateCreated;

  static const String tableName = 'plans';
  static const String columnId = 'id';
  static const String columnDetails = 'details';
  static const String columnNotes = 'notes';
  static const String columnStartTime = 'start_time';
  static const String columnEndTime = 'end_time';
  static const String columnIsReminderSet = 'is_reminder_set';
  static const String columnReminderSchedule = 'reminder_schedule';
  static const String columnDateScheduled = 'date_scheduled';
  static const String columnDateCreated = 'date_created';

  Map toMap() {
    Map map = <String, dynamic>{
      columnDetails: details,
      columnDateCreated: dateCreated,
      columnDateScheduled: dateScheduled,
      columnIsReminderSet: isReminderSet ? 1 : 0,
    };

    if (id != null) {
      map[columnId] = id;
    }

    if (notes != null) {
      map[columnNotes] = notes;
    }

    if (startTime != null) {
      map[columnStartTime] = startTime;
    }

    if (endTime != null) {
      map[columnEndTime] = endTime;
    }

    if (reminderSchedule != null) {
      map[columnReminderSchedule] = reminderSchedule;
    }

    return map;
  }

  bool hasChanged(Plan other) {
    return !compareTo(other);
  }

  bool compareTo(Plan other) {
    return id == other.id &&
        details == other.details &&
        notes == other.notes &&
        startTime == other.startTime &&
        endTime == other.endTime &&
        dateScheduled == other.dateScheduled &&
        isReminderSet == other.isReminderSet &&
        dateCreated == other.dateCreated;
  }

  Plan() : isReminderSet = false;

  Plan.fromMap(Map map) {
    id = map[columnId];
    details = map[columnDetails];
    notes = map[columnNotes];
    startTime = map[columnStartTime];
    endTime = map[columnEndTime];
    dateScheduled = map[columnDateScheduled];
    isReminderSet = map[columnIsReminderSet] == 1;
    reminderSchedule = map[columnReminderSchedule];
    dateCreated = map[columnDateCreated];
  }

  Plan.clone(Plan other) {
    id = other.id;
    details = other.details;
    notes = other.notes;
    startTime = other.startTime;
    endTime = other.endTime;
    dateScheduled = other.dateScheduled;
    isReminderSet = other.isReminderSet;
    reminderSchedule = other.reminderSchedule;
    dateCreated = other.dateCreated;
  }
}
