// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

import '../utils/strings.dart';

class Settings extends StatelessWidget {
  Settings({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed('/about');
            },
            child: ListTile(
              title: Text(
                Strings.appAbout,
                style: TextStyle(fontSize: 18.0),
              ),
            ),
          ),
          SwitchListTile(
            onChanged: (value) {},
            title: Text('Dark Theme'),
            value: false,
          ),
        ],
      ),
    );
  }
}
